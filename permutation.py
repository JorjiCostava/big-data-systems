from scipy import fft
import sys
import numpy as np

if len(sys.argv) < 2:
    print("Usage: python permutation.csv <my/dataset/dir>")
root = sys.argv[1]

ranks = []
for i in range(1000):
    values = np.loadtxt(f'{root}/agent_{i}.plans',
                        delimiter=',',
                        converters={0:lambda s:float(s.split(b':')[1])})
    rank = fft.dst(values, type=2).min(axis=1).mean()
    ranks.append(rank)

f = open('permutation.csv', 'w')
for line in map(str,reversed(np.array(ranks).argsort())):
    f.write(line + '\n')
f.close()
